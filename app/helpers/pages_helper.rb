# encoding: utf-8
module PagesHelper

  def title
    base_title = "Volare"
    if @meta_title.nil?
      base_title
    else
      "#{@meta_title} | #{base_title}"
    end
  end

  def metakey
    base_meta_key = "итальянские сапоги, сапоги из италии, сапоги италия, сапоги италия женские, обувь женская италия, сапоги женские италия"
    if @metakey.nil?
      base_meta_key
    else
      @metakey
    end
  end

  def metadesc
    base_meta_desc = "Модные и удобные женские сапоги из Италии"
    if @metadesc.nil?
      base_meta_desc
    else
      @metadesc
    end
  end

end
