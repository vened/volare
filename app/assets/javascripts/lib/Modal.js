var Modal = function (params) {
    "use strict";

    //INIT PARAMS
    var paramsDefault = {
        modalClass: "modal-default",
        buttonCloseView: true,
        modalResize: true,
        modalWidth: 700
    };
    params = $.extend({}, paramsDefault, params);

    var win = window,
        $win = $(win),
        $body = $('body'),
        sectionPage = $(".section-page"),
        $el = $(JST['layout/modal'](params)),
        $modalClose = $el.find('.modal-close');


    if (params.modalResize) {
        $win.on('resize', function () {
            _resize();
        });
    }


    $modalClose.on('click', function () {
        _destroy();
        return false;
    });

    function _show() {
        $body.append($el);
        return false;
    };

    function _resize() {
        var bodyHeight = $body.height(),
            Modal = $el.find('.modal'),
            ModalHeight = Modal.height(),
            positionTop = (bodyHeight - ModalHeight) / 2;
        if (ModalHeight > bodyHeight) {
            sectionPage.height(ModalHeight + 60);
            $el.height(ModalHeight + 60);
        } else {
            sectionPage.height($win.height());
            $el.height($win.height());
        }
        if (positionTop > 30)
            Modal.css("top", positionTop);
        return false;
    };

    function _destroy() {
        $el.detach();
        return false;
    };

    return {
        show: function () {
            _show();
            if (params.modalResize)
                _resize();
            return this;
        },
        resize: function () {
            _resize();
            return this;
        },
        destroy: function () {
            _destroy();
            return this;
        }
    };

};