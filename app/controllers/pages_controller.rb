class PagesController < ApplicationController
  include PagesHelper

  def index
    #@categories = Category.order("sortable ASC")
    @products = Product.where("home_show = 1").order("updated_at DESC")
  end

  def about
    @meta_title = "О компании"
  end

  def contacts
    @meta_title = "Контакты"
  end

  def fashion
    @meta_title = "Fashion Style/Lookbook"
  end

  def show
    @page = Page.find_by_path(params[:id])
    if @page.meta_title.nil?
      @meta_title = @page.title
    else
      @meta_title = @page.meta_title
    end
    @metakey = @page.meta_key
    @metadesc = @page.meta_desc
  end

end
