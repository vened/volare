class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories do |t|
      t.string :title
      t.text :description
      t.string :path
      t.integer :sortable

      t.timestamps
    end
  end
end
